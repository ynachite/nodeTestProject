var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

//Simple user schema consisting of username and userId
module.exports = new Schema({

  username: {
    type: String,
    unique: true,
    required: true
  },

  userId: {
    type: String,
    unique: true,
    required: true,
    index: true
  }

});
