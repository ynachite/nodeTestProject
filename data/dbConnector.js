var mongoose = require('mongoose'),
  userModel = mongoose.model('User', require('./models/userSchema'));

//Initiate db connection
var dbConn = function() {
  mongoose.connect('mongodb://localhost:27017/testDB');
  var db = mongoose.connection;
  db.on('error', function(err) {
    if (err) throw err;
    else throw 'connection error';
  });
};

//Add query functions
dbConn.prototype.getUser = function(userId, callback) {
  userModel.findOne({
    "userId": userId
  }, callback);
};

dbConn.prototype.createUser = function(username, userId, callback) {
  userModel.create({
    "username": username,
    "userId": userId
  }, callback);
};

module.exports = new dbConn();
