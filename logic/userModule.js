require('rootpath')();
var db = require('data/dbConnector');

var MAX_LENGTH = 25;

var userModule = function() {};

userModule.prototype.validateUsername = function(req, res, next) {
  if (!req.body || !req.body.username){
    return next(new Error("missing user information"));
  }

  if (req.body.username.length > MAX_LENGTH){
    return next(new Error("username too long"));
  }

  next();
};

userModule.prototype.getUser = function(req, res, next) {
  //check if userId present
  if (!req.params || !req.params.userId) {
    return next(new Error("missing userId"));
  } else {
    db.getUser(req.params.userId,
      function(err, value) {
        if (err) {
          return next(err);
        } else if(!value || !value.userId || !value.username){
          return next(new Error("User not found"));
        }else {
          res.json({
            "user": value
          });
        }
      }
    );
  }
};

userModule.prototype.createUser = function(req, res, next) {
  //check if user data present
  if (!req.body.userId) {
    return next(new Error("missing user information"));
  } else{
    db.createUser(req.body.username, req.body.userId,
      function(err, value) {
        if (err) {
          return next(err);
        } else if(!value || !value.userId){
          return next(new Error("User not found"));
        } else{
          res.json({
            "userId": value.userId
          });
        }
      });
  }
};

module.exports = new userModule();
