require('rootpath')();
require('it-each')({
  testPerIteration: true
});

var request = require('supertest'),
  expect = require("chai").expect,
  assert = require("chai").assert,
  path = require('path'),
  sinon = require('sinon'),
  mockery = require('mockery'),
  _ = require('lodash');


describe('Routing test suite', function() {

  var app, userModule;

  //Add API endpoints here
  var endpoints = [{
    "path": "/users",
    "method": "POST",
    "payload": {
      "username": "john_smith",
      "userId": "12345"
    },
    "headers": {},
    "expected": {
      "status": 200,
      "function": "createUser"
    }
  }, {
    "path": "/users",
    "method": "GET",
    "payload": "12345",
    "headers": {},
    "expected": {
      "status": 200,
      "function": "getUser"
    }
  }];

  before(function(done) {
    //enable mockery and register DB stub object
    mockery.enable({
      warnOnReplace: true,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('data/dbConnector', sinon.stub());

    //stub business logic module
    userModule = require("logic/userModule");

    //Use lodash function to get an array of all functions of userModule.
    //Replace all functions with stub functions that send a response back
    _.functions(userModule).forEach(function(element, index, array) {
      sinon.stub(userModule, element,
        function(req, res, next) {
          res.json({
            "method": req.method,
            "originalUrl": req.originalUrl,
            "baseUrl": req.baseUrl,
            "function": element
          });
        });

    });

    //start node app
    app = require('app');
    app.set('port', process.env.PORT || 3000);
    app.listen(app.get('port'));
    done();
  });

  after(function(done) {
    mockery.disable();
    done();
  });

  // perform async test
  describe('send endpoint requests', function() {
    it.each(endpoints, 'should check %s-request for endpoint %s', ['method', 'path'],
      function(endpt, done) {
        switch (endpt.method.toString().toLowerCase()) {
          case "post":
            request(app).post(endpt.path).set(endpt.headers).send(endpt.payload).end(
              function(err, res) {
                if (err) {
                  assert.fail(err.status, endpt.expected.status);
                } else {
                  console.log(res.text);
                  expect(res.status).to.equal(endpt.expected.status);
                  expect(res.body.function).to.equal(endpt.expected.function);
                  //Additional tests go here...
                }
                return done();
              });
            break;
          default:
            request(app).get(path.join(endpt.path, endpt.payload)).set(endpt.headers).end(
              function(err, res) {
                if (err) {
                  assert.fail(err.status, endpt.expected.status);
                } else {
                  console.log(res.text);
                  expect(res.status).to.equal(endpt.expected.status);
                  expect(res.body.function).to.equal(endpt.expected.function);
                  //Additional tests go here...
                }
                return done();
              });
            break;
        }
      });
  });
});
