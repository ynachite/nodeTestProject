require('rootpath')();

var expect = require("chai").expect,
  assert = require('chai').assert,
  mongoose = require('mongoose'),
  db = require('data/dbConnector'),
  userModel = mongoose.model('User', require('data/models/userSchema')),
  dbURI = 'mongodb://localhost:27017/testDB',
  initDB = require('lib/initDB')(dbURI),
  clearDB = require('mocha-mongoose')(dbURI, {
    noClear: true
  });

describe('DB test suite', function() {
  var myUsername = "john_smith";
  var myUserId = "12345";

  beforeEach(function(done) {
    clearDB(done);
  });

  after(function(done) {
    clearDB(done);
  });

  describe('user operations', function() {
    it("inserts a user", function(done) {
      db.createUser(myUsername, myUserId,
        function(reason, value) {
          if (reason) {
            done(reason);
          } else {
            expect(myUsername).to.equal(value.username);
            expect(myUserId).to.equal(value.userId);
            done();
          }
        });
    });

    it("fails to inserts the same user twice", function(done) {
      initDB("users", "test/data/users.json", true, function(err) {
        if (err) {
          done(err);
        } else {
          db.createUser(myUsername, myUserId,
            function(reason, value) {
              if (reason) {
                expect(reason.errmsg).to.have.string("duplicate key error");
                done();
              } else {
                assert.ok(false, "error rejecting same user twice");
                done(value);
              }
            });
        }
      });
    });

    it("gets a user", function(done) {
      initDB("users", "test/data/users.json", true, function(err) {
        if (err) {
          done(err);
        } else {
          db.getUser(myUserId,
            function(reason, value) {
              if (reason) {
                done(reason);
              } else {
                expect(myUsername).to.equal(value.username);
                expect(myUserId).to.equal(value.userId);
                done();
              }
            });
        }
      });
    });
  });
});
