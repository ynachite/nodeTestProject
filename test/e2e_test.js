require('rootpath')();

var request = require('supertest'),
  expect = require("chai").expect,
  assert = require("chai").assert,
  path = require('path'),
  sinon = require('sinon'),
  mockery = require('mockery'),
  dbURI = 'mongodb://localhost:27017/testDB',
  initDB = require('lib/initDB')(dbURI),
  userModule = require("logic/userModule"),
  app = require('app'),
  clearDB = require('mocha-mongoose')(dbURI, {
    noClear: true
  });

describe('End2end test suite', function() {

  before(function(done) {
    app.set('port', process.env.PORT || 3000);
    app.listen(app.get('port'));
    done();
  });

  beforeEach(function(done) {
    clearDB(done);
  });

  after(function(done) {
    clearDB(done);
  });

  // perform async test
  describe('send successful requests', function() {
    it("inserts new a user", function(done) {
      var payload = {
        "username": "john_smith",
        "userId": "12345"
      };
      request(app).post("/users").send(payload).end(
        function(err, res) {
          if (err) {
            assert.fail(err.status, "200");
          } else {
            expect(res.status).to.equal(200);
            expect(res.body.userId).to.equal("12345");
          }
          return done();
        });

    });

    it("retrieves new a user", function(done) {
      var endpoint = "/users";
      var userId = "12345";

      initDB("users", "test/data/users.json", true, function(err) {
        if (err) {
          done(err);
        } else {
          request(app).get(path.join(endpoint, userId)).end(
            function(err, res) {
              if (err) {
                assert.fail(err.status, endpt.expected.status);
              } else {
                expect(res.status).to.equal(200);
                expect(res.body.user.username).to.equal("john_smith");
                expect(res.body.user.userId).to.equal("12345");
              }
              return done();
            });
        }
      });
    });
  });
});
