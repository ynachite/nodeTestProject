require('rootpath')();

var request = require('supertest'),
  expect = require("chai").expect,
  assert = require("chai").assert,
  path = require('path'),
  sinon = require('sinon'),
  mockery = require('mockery'),
  dbURI = 'mongodb://localhost:27017/testDB',
  initDB = require('lib/initDB')(dbURI),
  userModule = require("logic/userModule"),
  app = require('app'),
  clearDB = require('mocha-mongoose')(dbURI, {
    noClear: true
  });

require('it-each')({
  testPerIteration: true
});

describe('Grouped End2end test suite', function() {

  before(function(done) {
    app.set('port', process.env.PORT || 3000);
    app.listen(app.get('port'));
    done();
  });

  // perform async test
  describe('tests not involving the db', function() {
    beforeEach(function(done) {
      clearDB(done);
    });

    //Add API endpoints here
    var endpoints = [{
      "path": "/users",
      "method": "POST",
      "payload": {
        "username": "john_smith",
        "userId": "12345"
      },
      "headers": {},
      "expected": {
        status: 200,
        evaluate: function(res) {
          expect(res.status).to.equal(200);
          expect(res.body.userId).to.equal("12345");
          //Additional tests go here...
        }
      }
    }];

    it.each(endpoints, 'should check %s-request for endpoint %s', ['method', 'path'], function(endpt, done) {
      request(app).post(endpt.path).set(endpt.headers).send(endpt.payload).end(
        function(err, res) {
          if (err) {
            assert.fail(err.status, endpt.expected.status);
          } else {
            endpt.expected.evaluate(res);
          }
          return done();
        });
    });
  });

  describe('tests involving db setup', function() {
    beforeEach(function(done) {
      clearDB(done);
    });

    after(function(done) {
      clearDB(done);
    });

    var endpoints = [{
      "path": "/users",
      "method": "GET",
      "payload": "12345",
      "headers": {},
      "testData": {
        "collection": "users",
        "filepath": "test/data/users.json"
      },
      "expected": {
        status: 200,
        evaluate: function(res) {
          expect(res.status).to.equal(this.status);
          expect(res.body.user.username).to.equal("john_smith");
          expect(res.body.user.userId).to.equal("12345");
          //Additional tests go here...
        }
      }
    }];

    // perform async test
    it.each(endpoints, 'should check %s-request for endpoint %s', ['method', 'path'],
      function(endpt, done) {
        initDB(endpt.testData.collection, endpt.testData.filepath, true, function(err) {
          if (err) {
            done(err);
          } else {
            request(app).get(path.join(endpt.path, endpt.payload)).set(endpt.headers).end(
              function(err, res) {
                if (err) {
                  assert.fail(err.status, endpt.expected.status);
                } else {
                  endpt.expected.evaluate(res);
                }
                return done();
              });
          }
        });
      });
  });
});
