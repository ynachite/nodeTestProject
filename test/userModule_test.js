require('rootpath')();

var expect = require("chai").expect,
  assert = require("chai").assert,
  mockery = require('mockery'),
  sinon = require('sinon');

describe('User_suite', function() {
  var userModule, db, res, req, userId, username;

  before(function(done) {
    //fill your db dummy return object
    username = "john smith";
    userId = 12345;

    //set up DB stub functions
    getUser = sinon.stub();
    getUser.callsArgWith(1, null, {
      "userId": userId,
      "username": username
    });

    createUser = sinon.stub();
    createUser.callsArgWith(2, null, {
      "userId": userId
    });

    db = {};
    db.getUser = getUser;
    db.createUser = createUser;

    //enable mockery and register DB stub object
    mockery.enable({
      warnOnReplace: true,
      warnOnUnregistered: false,
      useCleanCache: false
    });

    mockery.registerMock('data/dbConnector', db);

    //load userModule AFTER registering the DB stub so it does not load the real one
    userModule = require("logic/userModule");

    done();
  });

  after(function(done) {
    mockery.disable();
    done();
  });

  beforeEach(function(done) {
    //set up request and response
    req = {};
    res = {};
    done();
  });

  it("should throw an exception if username is too long", function(done) {
    req.body = {
      "username": "This username is too long!!!"
    };

    userModule.validateUsername(req, res, function(err) {
      if (err) {
        expect(err.message).to.equal("username too long");
        done();
      } else {
        done(new Error());
      }
    });
  });


  it("should create a user", function(done) {
    req.body = {
      "userId": userId,
      "username": username
    };

    //setup response assuming that you return the response via res.json
    res.json = function(respObj) {
      /* jshint ignore:start */
      expect(respObj).to.not.be.undefined;
      /* jshint ignore:end */
      expect(respObj.userId).to.equal(userId);
      //Additional tests go here...

      done();
    };

    userModule.createUser(req, res, done);
  });


  it("should get a user", function(done) {
    req.params = {
      "userId": userId
    };

    //setup response assuming that you return the response via res.json
    res.json = function(respObj) {
      expect(respObj.user).to.not.be.undefined;
      expect(respObj.user.username).to.equal(username);
      //Additional tests go here...

      done();
    };

    userModule.getUser(req, res, done);
  });

  it("should fail getting a user when no userId was passed", function(done) {
    req.params = {};

    res.json = function(respObj) {
      if (respObj) done(respObj);
      else done(new Error());
    };
    //setup response assuming that you return the response via res.json
    userModule.getUser(req, res, function(respObj) {
      expect(respObj).to.not.be.undefined;
      expect(respObj.message).to.equal("missing userId");
      //Additional tests go here...

      done();
    });
  });
});
