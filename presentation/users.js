require('rootpath')();

var express = require('express'),
  router = express.Router({
    mergeParams: true
  }),
  userModule = require("logic/userModule");

//Connect routes with module functions
router.post('/', userModule.createUser);
router.get('/:userId', userModule.getUser);

router.all('*', function(req, res, next) {
  return next(new Error());
});

module.exports = router;
