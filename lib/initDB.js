var fs = require('fs'),
  client = require('mongodb').MongoClient,
  EJSON = require('mongodb-extended-json'),
  _ = require('lodash');

module.exports = function(dbUri) {

  if (!(dbUri && _.isString(dbUri))) {
    throw new Error("initDB: Please call the module with a mongodb url.");
  }

  return function(collectionName, filePath, clearExisting, done) {
    //Make sure done is a callback function
    if(!(done && typeof done == 'function')){
      done = function(err){
        if(err)throw err;
      };
    }

    if (!(filePath && _.isString(filePath))) {
      done(new Error("initDB: Please insert a valid filepath."));
    }

    //default collection name is same as filename
    if (!collectionName) {
      collectionName = filePath.split('\\').pop().split('/').pop();
      collectionName = collectionName.substring(0, collectionName.indexOf("."));
    }

    fs.readFile(filePath, 'utf8', function(err, data) {

      if (err) {
        return done(new Error("initDB: Failed to read file " + filepath));
      }

      client.connect(dbUri, function(err, db) {
        if (err) {
          return done(err);
        }

        //turn mongoexport data format into a string that can be parsed to an array
        data = data.trim().split(/\r\n|\r|\n/).toString();
        data = "["+data+"]";

        insertCollection(db, collectionName, data , clearExisting, done);
      });
    });
  };
};

function insertCollection(db, collectionName, data, clearExisting, done) {
  var json = EJSON.parse(data);
  var collection = db.collection(collectionName);

  if (clearExisting) {
    collection.removeMany();
  }

  collection.insert(json, function(err, doc) {
    db.close();
    if (err) {
      return done(err);
    }else{
      done();
    }
  });
}
