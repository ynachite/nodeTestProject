//SETUP IMPORTS
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var users = require("presentation/users");

// SETUP APP
var app = express();
app.locals.title = "My first Express app";
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

// SETUP ROUTING
app.use('/users', users);

// ERROR HANDLING
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    title: 'error'
  });
});

module.exports = app;
